﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Materiale_Substante
{
    public partial class SecondForm : Form
    {
        //parent form
        public Form1 parentForm { set; get; }

        //image locations
        private string firstMovie = System.Environment.CurrentDirectory + "/Resources/film1.mp4";
        private string secondMovie = System.Environment.CurrentDirectory + "/Resources/potions.mp4";
        private string thirdMovie = System.Environment.CurrentDirectory + "/Resources/materials.mp4";

        //image count
        private int nr_image = 1;

        //map with key= nr_image and value= path to image
        Dictionary<int, string> images_locations = new Dictionary<int, string>();

        //web player
        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

        public SecondForm()
        {
            InitializeComponent();
 
            images_locations.Add(1, thirdMovie);
            images_locations.Add(2, secondMovie);
            images_locations.Add(3, firstMovie);
            images_locations.Add(4, "");
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            //back to the main screen
            reset();
            this.Hide();
            parentForm.Show();
        }

        private void button_next_Click(object sender, EventArgs e)
        {


            if (nr_image <= images_locations.Count)
            {
                axWindowsMediaPlayer1.Ctlcontrols.stop();
                axWindowsMediaPlayer1.URL = images_locations[nr_image];
                nr_image++;
            }

            if(nr_image > images_locations.Count)
            {
                reset();
                ThirdForm thirdForm = new ThirdForm();
                thirdForm.parentForm = this;
                this.Owner = thirdForm;
                this.Hide();

                thirdForm.Show();
            }
        }

        private void reset()
        {
            nr_image = 1;
            axWindowsMediaPlayer1.Ctlcontrols.stop();
        }

        private void axWindowsMediaPlayer1_Enter(object sender, EventArgs e)
        {
           
            axWindowsMediaPlayer1.Ctlcontrols.play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reset();
            ThirdForm thirdForm = new ThirdForm();
            thirdForm.parentForm = this;
            this.Owner = thirdForm;
            this.Hide();

            thirdForm.Show();
        }
    }
}
