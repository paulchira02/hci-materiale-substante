﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Materiale_Substante
{
    public partial class ImagesForm : Form
    {

        //replace with your project path
        private string path = System.Environment.CurrentDirectory;

        //web player
        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

        public ImagesSubstancesForm parentForm { set; get; }

        public ImagesForm()
        {
            InitializeComponent();

            pictureBox1.ImageLocation = path + "/Resources/" + "lemn.jpg";
            pictureBox2.ImageLocation = path + "/Resources/" + "piatra2.jpg";
            pictureBox3.ImageLocation = path + "/Resources/" + "ceramica2.gif";
            pictureBox4.ImageLocation = path + "/Resources/" + "sticla.png";
            pictureBox5.ImageLocation = path + "/Resources/" + "metal.jpg";

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "lemnul.mp4";
            wplayer.controls.play();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "piatra.mp4";
            wplayer.controls.play();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "ceramica.mp4";
            wplayer.controls.play();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "sticla.mp4";
            wplayer.controls.play();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "metal.mp4";
            wplayer.controls.play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wplayer.controls.stop();
            Form1 form1 = new Form1();
            form1.parentForm = this;
            this.Owner = form1;
            this.Hide();

            form1.Show();
        }

        private void MATERIALE_Click(object sender, EventArgs e)
        {

        }

        private void button_back_Click(object sender, EventArgs e)
        {
            //back to the main screen
            wplayer.controls.stop();
            this.Hide();
            parentForm.Show();
        }
    }
}
