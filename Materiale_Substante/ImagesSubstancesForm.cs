﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Materiale_Substante
{
    public partial class ImagesSubstancesForm : Form
    {
        private string path = System.Environment.CurrentDirectory;
        //web player
        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

        public Form1 parentForm { set; get; }

        public ImagesSubstancesForm()
        {
            InitializeComponent();
            pictureBox1.ImageLocation = path + "/Resources/" + "salt.jpg";
            pictureBox2.ImageLocation = path + "/Resources/" + "dioxid3.jpg";
            pictureBox3.ImageLocation = path + "/Resources/" + "clor.jpg";
            pictureBox4.ImageLocation = path + "/Resources/" + "water.jpg";
            pictureBox5.ImageLocation = path + "/Resources/" + "oxi.png";

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox4.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "sarea.mp4";
            wplayer.controls.play();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "dioxid.mp4";
            wplayer.controls.play();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "clorul.mp4";
            wplayer.controls.play();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "apa.mp4";
            wplayer.controls.play();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            wplayer.URL = path + "/Resources/" + "oxigen.mp4";
            wplayer.controls.play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wplayer.controls.stop();
            ImagesForm imagesForm = new ImagesForm();
            imagesForm.parentForm = this;
            this.Owner = imagesForm;
            this.Hide();

            imagesForm.Show();
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            //back to the main screen
            this.Hide();
            parentForm.Show();
        }
    }
}
