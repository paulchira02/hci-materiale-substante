﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Materiale_Substante
{
    public partial class Form1 : Form
    {
        private string path = System.Environment.CurrentDirectory;


        //questions buttons from ui that are clicked
        private bool question1 = false;
        private bool question2 = false;
        private bool question3 = false;
        private bool question4 = false;
        private bool question5 = false;

        public ImagesForm parentForm { set; get; }

        public Form1()
        {
            InitializeComponent();

            //make picturebox invisible at start
            pictureBox1.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(pictureBox1.Visible == false)
            {
                pictureBox1.Visible = true;
            }
            pictureBox1.ImageLocation = path + "/Resources/" + "teacher_chemicals.gif";
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            button1.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "tick_button.png");
            openNewWindow("question1");
            // disable button click
            button1.Click -= button1_Click;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Visible == false)
            {
                pictureBox1.Visible = true;
            }
            pictureBox1.ImageLocation = path + "/Resources/" + "anim.gif";
            button2.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "tick_button.png");
            openNewWindow("question2");
            // disable button click
            button2.Click -= button2_Click;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Visible == false)
            {
                pictureBox1.Visible = true;
            }
            pictureBox1.ImageLocation = path + "/Resources/" + "anim_gif.gif";
            button3.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "tick_button.png");
            openNewWindow("question3");
            // disable button click
            button3.Click -= button3_Click;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Visible == false)
            {
                pictureBox1.Visible = true;
            }
            pictureBox1.ImageLocation = path + "/Resources/" + "bottle_chemicals.gif";
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            button4.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "tick_button.png");
            openNewWindow("question4");
            // disable button click
            button4.Click -= button4_Click;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Visible == false)
            {
                pictureBox1.Visible = true;
            }
            pictureBox1.ImageLocation = path + "/Resources/" + "teacher_chemicals.gif";
            button5.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "tick_button.png");
            // disable button click
            button5.Click -= button5_Click;
            openNewWindow("question5");
 
        }

        private void openNewWindow(String question)
        {
            if ("question1".Equals(question))
            {
                question1 = true;
            }
            if ("question2".Equals(question))
            {
                question2 = true;
            }
            if ("question3".Equals(question))
            {
                question3 = true;
            }
            if ("question4".Equals(question))
            {
                question4 = true;
            }
            if ("question5".Equals(question))
            {
                question5 = true;
            }

            if (question1 == true && question2 == true && question3 == true && question4 == true && question5 == true)
            {
                reset();
                SecondForm secondForm = new SecondForm();
                secondForm.parentForm = this;
                this.Owner = secondForm;
                this.Hide();

                secondForm.Show();
            }
        }

        //reset values
        private void reset()
        {
            question1 = question2 = question3 = question4 = question5 = false;

            button1.Click += button1_Click;
            button2.Click += button2_Click;
            button3.Click += button3_Click;
            button4.Click += button4_Click;
            button5.Click += button5_Click;

            button1.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "questionmark.png");
            button2.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "questionmark.png");
            button3.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "questionmark.png");
            button4.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "questionmark.png");
            button5.Image = System.Drawing.Image.FromFile(path + "/Resources/" + "questionmark.png");

            pictureBox1.Visible = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            SecondForm secondForm = new SecondForm();
            secondForm.parentForm = this;
            this.Owner = secondForm;
            this.Hide();

            secondForm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //back to the main screen
            reset();
            this.Hide();
            parentForm.Show();
        }
    }
}
